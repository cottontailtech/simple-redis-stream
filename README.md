# Simple Redis Stream

## Installation

To install the necessary dependencies, execute the following command:

```bash
npm i @cotton-tail-tech/simple-redis-stream
```

## Usage

### Import

Import the module into your Node.js application:

```javascript
const srs = require("@cotton-tail-tech/simple-redis-stream");
```

### Initialization

Initialize the Redis connection with the following parameters:

```javascript
srs.init({
  host: "localhost",
  port: 6379,
});
```

### Subscribe to Events

Subscribe to specific events by providing the event name and a callback function to handle incoming messages:

```javascript
srs.subscribe("zendesk_api_create", (message) => {
  console.log(message);
});
```

### Publish Events

Publish events to the Redis stream with a designated event name and associated data:

```javascript
srs.publish("zendesk_api_create", { id: 1, name: "test" });
```

This readme provides a concise overview of how to set up and utilize the Simple Redis Stream module for integrating Redis streams into your Node.js applications.