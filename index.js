const Redis = require("ioredis");
const { EventEmitter } = require("events");
/**
 * Global variables for Redis clients
 */
let redis = undefined;
let sub = undefined; 

/**
 * Module exports
 */
module.exports = {
  /**
   * Initialize Redis client and duplicate it for subscriptions
   * @param {Object} config - Redis client configuration
   * 
   */
  init: function (config) {
    redis = new Redis(config);
    sub = redis.duplicate(); // Create a duplicate of the Redis client for subscriptions
  },
  /**
   * Publish a message to a Redis stream
   * @param {String} topic - Stream name
   * @param {Object} message - Message to be published
   */
  publish: async function (topic, message) {
    await redis.xadd(topic, '*', 'message', JSON.stringify(message));
  },
  /**
   * Subscribe to a Redis stream and listen for messages
   * @param {String} topic - Stream name
   * @param {Function} callback - Callback function to handle messages
   */
  subscribe: function (topic, callback) {
    const streamListener = new RedisStreamListener(topic);
    streamListener.on(topic, callback);
    streamListener.subscribe();
  },
  /**
   * Close Redis clients
   */
  close: function () {
    redis.quit();
  }
};

/**
 * Class to listen for Redis stream messages
 */
class RedisStreamListener extends EventEmitter {
  /**
   * Constructor for RedisStreamListener
   * @param {String} stream - Stream name
   */
  constructor(stream) {
    super();
    this.stream = stream;
  }

  /**
   * Listen for stream messages and emit them
   * @param {String} lastId - Last message ID
   */
  async listenForMessage(lastId = "$") {
    const results = await sub.xread("BLOCK", 0, "STREAMS", this.stream, lastId);
    const [key, messages] = results[0]; // `key` equals to "user-stream"

    messages.forEach((message) => {
      this.emit(this.stream, JSON.parse(message[1][1]));
    });

    // Remove the processed messages from the stream
    await sub.xdel(this.stream, ...messages.map((m) => m[0]));

    // Pass the last id of the results to the next round.
    await this.listenForMessage(messages[messages.length - 1][0]);
  }

  /**
   * Subscribe to the stream and start listening for messages
   */
  subscribe() {
    this.listenForMessage();
  }
}