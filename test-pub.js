const srs = require(".");

srs.init({
  host: "dockge.prague.local",
  port: 6379,
});

srs.publish("zendesk_api_create", { id: 1, name: "test" });
srs.publish("zendesk_api_create", { id: 2, name: "test2" });
srs.publish("zendesk_api_create", { id: 3, name: "test3" });

srs.close();